FROM php:7.4.1-apache

### Composer dependencies
RUN apt update && apt install -y git unzip
RUN docker-php-ext-install pcntl

### Download & install composer
RUN curl -sS https://getcomposer.org/installer | php && mv composer.phar /usr/local/bin/composer

COPY . .

RUN composer install