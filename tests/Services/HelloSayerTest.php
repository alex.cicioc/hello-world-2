<?php

use App\Services\HelloSayer;

class HelloSayerTest extends \PHPUnit\Framework\TestCase
{
    public function test_sayHelloTest()
    {
        $this->assertIsString(HelloSayer::sayHello('vasile'));
    }
}
