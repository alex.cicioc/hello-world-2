<?php

namespace App\Services;

class HelloSayer
{
    public static function sayHello(string $name): string
    {
        return "hello $name";
    }
}